import logging
import re
import urllib.parse

from bs4 import BeautifulSoup
from datetime import date
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import postgres_service

# класс для захвата новостей Медузы непосредственно с Web-страницы
# выполнен в качестве тестового задания
# для правильной работы с новостями Медузы необходимо использовать их API: https://habr.com/ru/post/259471/
class MeduzaGrabber:
    driver = None
    button_show_more = None
    meduza_page = None

    # возвращает оптимальные настройки для безголового браузера
    def _get_chrome_options_(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        chrome_options.experimental_options["prefs"] = chrome_prefs
        chrome_prefs["profile.default_content_settings"] = {"images": 2} # отключает изображения
        return chrome_options

    # получает дату из URL новости
    def _get_news_timestamp_(self, url):
        pattern = r'https:\/\/meduza\.io\/.+\/(\d{4})\/(\d{2})\/(\d{2})'
        match = re.match(pattern, url)
        if match:
            try:
                timestamp = date(int(match.group(1)), int(match.group(2)), int(match.group(3)))
            except ValueError:
                return None
            return timestamp
        return None
    
    # заново читает загруженную страницу
    def _refresh_meduza_page_(self):
        source_data = self.driver.page_source
        soup = BeautifulSoup(source_data, 'html.parser')
        self.meduza_page = soup('a', class_='ChronologyItem-link')

    # имитирует нажатие на кнопку "Показать ещё"
    def show_more(self):
        if not self.driver is None and not self.button_show_more is None:
            self.driver.execute_script("arguments[0].click();", self.button_show_more)
            self._refresh_meduza_page_()

    # выполняет первоначальное открытие Медузы
    def open_meduza(self, url):
        # открывает браузер Chrome через Selenium,
        # выполняет переключение в режим отображения новостей по хронологии,
        # обновляет страницу, находит и сохраняет кнопку "Показать ещё"
        self.driver = webdriver.Chrome(options=self._get_chrome_options_())
        self.driver.get(url)
        self.driver.execute_script("localStorage.setItem('MDZ_screenViewType', '\"LIST\"');")
        self.driver.refresh()
        try:
            self.button_show_more = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//button[text()="Показать еще"]')))
        except:
            logging.error('Show more button wasn''t found.')
        self._refresh_meduza_page_()

def __main__():
    logging.basicConfig(level=logging.INFO)
    logging.info('News grabbing started...')
    url = 'https://meduza.io/'
    grabber = MeduzaGrabber()
    grabber.open_meduza(url)
    logging.info('Connecting to database...')
    db_service = postgres_service.PostgresService() # подключение к СУБД

    batch = []              # для накопления новостей, пожлежащих сохранению в БД
    saved_links = []        # новости за текущую дату, уже сохранённые в БД
    processed_urls = []     # ссылки, уже обработанные
    search_finished = False

    # Основной цикл процессинга страницы новостей
    # Алгоритм:
    # * Получаем URL. Если его уже обрабатывали, пропускаем
    # * Из URL получаем дату. Если началась следующая дата, получаем из БД перечень имеющихся новостей за эту дату
    # * Если текущая новость уже есть среди новостей за этот день, значит, мы обработали все свежие новости. Или мы нашли уже 1000 свежих новостей (первоначальное заполнение)
    # * Получаем заголовок новости. Добавляем реквизиты новости в список для сохранения в БД
    # * Если страница кончилась, но не все свежие новости обработаны, "нажимаем" на кнопку "Показать ещё"
    logging.info('Starting news processing...')
    while not search_finished:
        current_timestamp = None
        for tag in grabber.meduza_page:
            if hasattr(tag, 'attrs') and 'href' in tag.attrs:
                news_url = urllib.parse.urljoin(url, tag['href'])
                if news_url in processed_urls:
                    continue
                processed_urls.append(news_url)
                news_timestamp = grabber._get_news_timestamp_(news_url)
                if news_timestamp is None:
                    logging.debug('URL %s -- can''t parse timestamp! Ignored.', news_url)
                    continue
                if current_timestamp != news_timestamp:
                    logging.info('Processing %s', news_timestamp)
                    result = db_service.get_news_by_day(news_timestamp)
                    if result is None:
                        logging.critical('Aborting due to database issues...')
                        return
                    saved_links = [ link for (link, ) in result ]
                    current_timestamp = news_timestamp
                if news_url in saved_links or len(batch) > 999:
                    logging.info('Reached existing news, stopping search.')
                    search_finished = True
                    break
                header_tag = tag.find('strong') #tag.find('div', class_='ChronologyItem-header')
                if hasattr(header_tag, 'text'):
                    news_header = header_tag.text
                    batch.append((news_url, news_header, news_timestamp))
        if not search_finished:
            logging.info('Reached the end of page, requesting more news. News collected: %s', len(batch))
            grabber.show_more()

    # Если свежие новости нашлись, сохраняем их в БД пачкой
    if len(batch) > 0:
        logging.info('%s news collected, updating database.', len(batch))
        db_service.insert_bulk(batch)
        logging.info('Update complete, quitting.')
    else:
        logging.info('No news collected, quitting.')

if __name__ == "__main__":
    __main__()