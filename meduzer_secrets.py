import os

def get_db_host():
    return os.environ.get('DB_HOST')

def get_db_name():
    return os.environ.get('DB_NAME')

def get_db_username():
    return os.environ.get('DB_USERNAME')

def get_db_password():
    return os.environ.get('DB_PASSWORD')