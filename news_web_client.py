import aiohttp
import asyncio

# тестовый клиент для сервера JSON API
async def __main__():
    async with aiohttp.ClientSession() as session:
        async with session.get('http://localhost:8080/', json={'request_type': 'news', 'parameters': {'date_start': "2021-07-14 00:00:00", 'date_end': "2021-07-14 23:59:59"}}) as resp:
            print(resp.status)
            print(await resp.text())

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(__main__())