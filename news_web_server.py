import logging

from aiohttp import web
from datetime import datetime

import postgres_service

# Сервер JSON API
# Формат входящего JSON:
# Словарь;
# 'request_type' (строка): тип запроса. Допустимые значения: 'news' -- новости
# 'parameters' (словарь): параметры запроса:
#       'date_start' (строка с датой формата 'гггг-ММ-дд чч:мм:сс'): начало периода
#       'date_end' (строка с датой формата 'гггг-ММ-дд чч:мм:сс'): конец периода
# Для отбора по периоду обе даты должны быть корректно заполнены, иначе будут выгружены все новости
# Формат исходящего JSON:
# Список словарей:
# 'link' (строка): URL новости
# 'headline' (строка): заголовок новости
# 'posted_on' (строка с датой формата 'гггг-ММ-дд чч:мм:сс'): дата новости
async def json_api(request: web.Request):
    request_data = await request.json()
    if isinstance(request_data, dict):
        request_type = request_data.get('request_type')
        if request_type == 'news':
            params = request_data.get('parameters')
            date_start = None
            date_end = None
            if isinstance(params, dict):
                try:
                    date_start = datetime.strptime(params.get('date_start'), '%Y-%m-%d %H:%M:%S')
                    date_end = datetime.strptime(params.get('date_end'), '%Y-%m-%d %H:%M:%S')
                except:
                    pass
            db_service = postgres_service.PostgresService()
            result = db_service.get_news(date_start, date_end)
            if result is None or len(result) == 0:
                return web.json_response(None)
            keys = ['link', 'headline', 'posted_on']
            dict_result = [ dict(zip(keys, (link, header, str(post_date)))) for (link, header, post_date) in result ]
            return web.json_response(dict_result)
        elif request_type is None:
            return None
        else:
            logging.info('Unknown request type: %s', request_type)

def __main__():
    logging.basicConfig(level=logging.INFO)
    app = web.Application()
    app.add_routes([web.get('/', json_api)])
    web.run_app(app)

if __name__ == '__main__':
    __main__()