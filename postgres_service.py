from datetime import date
import logging
import psycopg2
import psycopg2.extras

import meduzer_secrets as secrets

# Класс для работы с PostgreSQL
class PostgresService:
    connection = None
    host = None
    database = None
    username = None
    password = None

    # при создании объекта получаем секреты
    def __init__(self):
        self.host = secrets.get_db_host()
        self.database = secrets.get_db_name()
        self.username = secrets.get_db_username()
        self.password = secrets.get_db_password()
    
    # при удалении объекта разрываем соединение с СУБД
    def __del__(self):
        if not self.connection is None:
            self.connection.close()
    
    # подключение к СУБД
    def _connect_(self):
        if self.connection is None:
            self.connection = psycopg2.connect(host=self.host, database=self.database, user=self.username, password=self.password)
    
    # принимает запрос и кортеж с параметрами
    # возвращает список кортежей с результатами запроса
    def _fetch_query_result_(self, query, parameters):
        cur = None
        result = None
        try:
            self._connect_()
            cur = self.connection.cursor()
            cur.execute(query, parameters)
            result = cur.fetchall()
            cur.close()
        except Exception as error:
            logging.error(error)
        finally:
            if not cur is None:
                cur.close()
            return result
    
    # пакетно сохраняет данные в БД
    def insert_bulk(self, batch: dict):
        cur = None
        try:
            self._connect_()
            cur = self.connection.cursor()
            psycopg2.extras.execute_values(cur, 'INSERT INTO news (link, headline, posted_on) VALUES %s', batch)
            self.connection.commit()
            cur.close()
        except psycopg2.DatabaseError as error:
            logging.error(error)
        finally:
            if not cur is None:
                cur.close()
        if not self.connection is None:
            self.connection.close()
    
    # получает перечень новостных ссылок за день
    def get_news_by_day(self, date: date):
        query = '''
            select n.link
            from news n
            where date_trunc('day', n.posted_on) =  %s
            '''
        parameters = (date, )
        return self._fetch_query_result_(query, parameters)
    
    # получает перечень новостей (если указан период, то за этот период)
    def get_news(self, date_start = None, date_end = None):
        query = '''
            select n.link, n.headline, n.posted_on
            from news n
            '''
        if not date_start is None and not date_end is None:
            query += '''
                where n.posted_on between %s and %s
                '''
            parameters = (date_start, date_end)
        else:
            parameters = None
        return self._fetch_query_result_(query, parameters)